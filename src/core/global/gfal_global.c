/*
 * Copyright (c) CERN 2013-2015
 *
 * Copyright (c) Members of the EMI Collaboration. 2010-2013
 *  See  http://www.eu-emi.eu/partners for details on the copyright
 *  holders.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <glib.h>
#include <global/gfal_global.h>
#include <common/gfal_common_internal.h>

GQuark gfal2_get_core_quark()
{
    return g_quark_from_static_string(GFAL2_QUARK_CORE);
}


GQuark gfal2_get_config_quark()
{
    return g_quark_from_static_string(GFAL2_QUARK_CONFIG);
}


GQuark gfal2_get_plugins_quark()
{
    return g_quark_from_static_string(GFAL2_QUARK_PLUGINS);
}


gfal2_context_t gfal2_context_new(GError ** err)
{
    return gfal_initG(err);
}


void gfal2_context_free(gfal2_context_t context)
{
    gfal_handle_freeG(context);
}


gchar** gfal2_get_plugin_names(gfal2_context_t context)
{
    gchar** array = g_new0(gchar*, context->plugin_opt.plugin_number + 1);
    int i;

    for (i = 0; i < context->plugin_opt.plugin_number; ++i) {
        array[i] = g_strdup(context->plugin_opt.plugin_list[i].getName());
    }
    array[i] = NULL;

    return array;
}


gfal2_context_t gfal_context_new(GError ** err)
{
    return gfal2_context_new(err);
}


void gfal_context_free(gfal2_context_t context)
{
    return gfal2_context_free(context);
}
